/*
 *     SocialLedge.com - Copyright (C) 2013
 *
 *     This file is part of free software framework for embedded processors.
 *     You can use it and/or distribute it as long as this copyright header
 *     remains unmodified.  The code is free for personal use and requires
 *     permission to use in a commercial product.
 *
 *      THIS SOFTWARE IS PROVIDED "AS IS".  NO WARRANTIES, WHETHER EXPRESS, IMPLIED
 *      OR STATUTORY, INCLUDING, BUT NOT LIMITED TO, IMPLIED WARRANTIES OF
 *      MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE APPLY TO THIS SOFTWARE.
 *      I SHALL NOT, IN ANY CIRCUMSTANCES, BE LIABLE FOR SPECIAL, INCIDENTAL, OR
 *      CONSEQUENTIAL DAMAGES, FOR ANY REASON WHATSOEVER.
 *
 *     You can reach the author of this software at :
 *          p r e e t . w i k i @ g m a i l . c o m
 */

/**
 * @file
 * This contains the period callback functions for the periodic scheduler
 *
 * @warning
 * These callbacks should be used for hard real-time system, and the priority of these
 * tasks are above everything else in the system (above the PRIORITY_CRITICAL).
 * The period functions SHOULD NEVER block and SHOULD NEVER run over their time slot.
 * For example, the 1000Hz take slot runs periodically every 1ms, and whatever you
 * do must be completed within 1ms.  Running over the time slot will reset the system.
 */

#include <conductance.hpp>
#include "temperatureADC.hpp"
#include "lowPassFilter.hpp"
#include <stdint.h>
#include "io.hpp"
#include "periodic_callback.h"
#include "BLE.hpp"
#include "stdio.h"
#include "adc0.h"
#include "mapping.hpp"
#include "tlm/c_tlm_comp.h"
#include "tlm/c_tlm_var.h"
#include "math.h"
#include "uart3.hpp"

/// This is the stack size used for each of the period tasks
const uint32_t PERIOD_TASKS_STACK_SIZE_BYTES = (512 * 4);

bool startFlag = 0, tFlag = 0;
float_t temp, filteredTemp, volt, filteredVolt, salt, SJoneTemp;
char recieveArray;
uint16_t rawConductance, rawTemp;
uint16_t var1 = 0, var2 = 0;
bool calc = false;
uint8_t counter = 0;
static float_t prevVolt, prevTemp;

bluetooth &bluetoothInstance = bluetooth::getInstance();
map3D patternRecg;
tempADC compute_ADC;
LPC_Filter lpf;

Uart3 &temperature = Uart3::getInstance();

/// Called once before the RTOS is started, this is a good place to initialize things once
bool period_init(void)
{
	adc0_init();
	compute_ADC.adc_init();
	initConductanceADC();
	temperature.init(9600, 100, 100);

	bluetoothInstance.initBLE();
    return true; // Must return true upon success
}

/// Register any telemetry variables
bool period_reg_tlm(void)
{
	tlm_component *smartTongue = tlm_component_add("Salinity");

	TLM_REG_VAR(smartTongue, rawConductance, tlm_uint);
	TLM_REG_VAR(smartTongue, volt, tlm_float);
	TLM_REG_VAR(smartTongue, filteredVolt, tlm_float);
	TLM_REG_VAR(smartTongue, recieveArray, tlm_string);
	TLM_REG_VAR(smartTongue, rawTemp, tlm_uint);
	TLM_REG_VAR(smartTongue, temp, tlm_float);
	TLM_REG_VAR(smartTongue, filteredTemp, tlm_float);
	TLM_REG_VAR(smartTongue, salt, tlm_float);
	TLM_REG_VAR(smartTongue, calc, tlm_bit_or_bool);
	TLM_REG_VAR(smartTongue, SJoneTemp, tlm_float);
	return true;
}

void period_1Hz(void)
{
	bool ok;
	float tempCalc = 0.0;
	char temp_arr[6];
	for(int i =0; i < 6; i ++)
		temp_arr[i] = 0;
	ok = temperature.gets(temp_arr, 5, 10);
	printf("val = %s\n", temp_arr);

	if(temp_arr[2] == '.')
	{
		tempCalc = (((int)temp_arr[0] - 48) * 10) + ((int)temp_arr[1] - 48) +
				((int)temp_arr[3] - 48) / 10.0 + ((int)temp_arr[4] - 48) / 100.0;
	}
	if(tempCalc > 5 && tempCalc < 100)
	{
		temp = tempCalc;
	}

	//sprintf()
	printf("temp: %f, cond: %f, salt: %f\n", filteredTemp, filteredVolt, salt);

	//printf("Conductance value:%d, Voltage:%f\n",readConductance(),convertToVolt(readConductance()));
}

void period_10Hz(void)
{
	bool ok = false;
	typedef enum {
		start_reset,
		calculating,
		displayTemp,
		displayCond,
		displaySalinity,
		calculated
	}calculationMode_t;

	static calculationMode_t calcMode = start_reset;

	switch (calcMode) {

	case start_reset:
		calcMode = displayTemp;
		filteredTemp = 0;
		filteredVolt = 0;
		salt = 0.0;
		switchOffLightsReset();
		bluetoothInstance.sendChar('N');
		//printf("start\n");
		break;

	case displayTemp:
		calcMode = displayCond;
		ok = bluetoothInstance.sendTemp(filteredTemp);
		//printf("temp : %d\n", ok);
		break;

	case displayCond:
		calcMode = displaySalinity;
		bluetoothInstance.sendCond(filteredVolt);
		//printf("cond\n");
		break;

	case displaySalinity:
		if (startFlag)
			calcMode = calculating;
		else
			calcMode = start_reset;
		bluetoothInstance.sendSalinity(salt);
		break;

	case calculating:
		calcMode = displayTemp;
		blinkLightOnCalculating();

		rawConductance = readConductance();
		volt = convertToVolt(rawConductance);
		filteredVolt = lpf.filter(volt, &prevVolt);

		//filteredTemp = lpf.filter(temp, &prevTemp);
		filteredTemp = temp;

		//first input will be voltage and second will be temperature
		salt = patternRecg.mappingFunc(filteredVolt, filteredTemp);

		bluetoothInstance.sendChar('S');

		//if(lpf.saltLevel(salt))
		counter++;

		if(counter > 100)
		{
			counter = 0;
			calcMode = calculated;
		}

		break;

	case calculated:
		if(startFlag)
			calcMode = calculated;
		else
			calcMode = start_reset;

		switchOnLightsAfterCalc();
		bluetoothInstance.sendChar('D');
		break;

	default:
		calcMode = start_reset;
		break;
	}

}

void period_100Hz(void)
{
	recieveArray = bluetoothInstance.recieveFromBLE();

	if(recieveArray == 's' || recieveArray == 'S')
			startFlag = !startFlag;

}

void period_1000Hz(void)
{
    //LE.toggle(4);
}
