/*
 * lowPassFilter.hpp
 *
 *  Created on: Nov 30, 2015
 *      Author: Tejeshwar
 */

#ifndef L5_APPLICATION_LOWPASSFILTER_HPP_
#define L5_APPLICATION_LOWPASSFILTER_HPP_


class LPC_Filter//: public SingletonTemplate<LPC_Filter>
{
	private:
		//LPC_Filter();
		//friend class SingletonTemplate <LPC_Filter>;
		float dataToFilter;
		float lpfBeta = 0.025; // 0<�<1
		float dataToBLE = 0;
		float compareVar = 0;

	public:
		float filter(float data, float *prevData);
		bool saltLevel(float saltData);
};




#endif /* L5_APPLICATION_LOWPASSFILTER_HPP_ */
