/*
 * mapping.hpp
 *
 *  Created on: Nov 30, 2015
 *      Author: Abhi
 */

#ifndef L5_APPLICATION_MAPPING_HPP_
#define L5_APPLICATION_MAPPING_HPP_

#include "math.h"
#include "stdint.h"

#define NUMBER_OF_TEMP	9
#define NUMBER_OF_COND	10


class map3D {
public:
// function to map output based on the input
float_t mapOutput(float_t input, float_t lowInput, float_t highInput, float_t lowOutput , float_t highOutput);

// function to compare and get the nearest column index.
uint8_t getLowerRangeColumnIndex(float_t value);

// function to comapre and get the nearest row index.
uint8_t getLowerRangeRowIndex(float_t value);

// function to map two inputs to a single output. Pattern recognition algorithm
float_t mappingFunc(float_t rowValue, float_t columnValue);

private:
float_t salinity_percent[NUMBER_OF_TEMP][NUMBER_OF_COND] =
{
		{0.0, -300.0,	-55.0, 	10.0, 22.0, 35.0, 46.0, 53.0},
		{0.0,   0.0   ,	0.0  , 	0.0 , 0.0 , 0.0 , 0.0 , 0.0 },
		{0.06, 0.0   ,	0.0  , 	0.0 , 0.0 , 0.0 , 0.0 , 0.0 },
		{0.15, 0.0   ,	0.0  , 	0.0 , 0.0 , 1.6 , 0.0 , 0.0 },
		{0.2,  0.0   ,	0.0  , 	0.0 , 1.8 , 1.5 , 1.9 , 1.8 },
		{0.25, 0.0   ,	0.0  , 	0.0 , 0.3 , 1.2 , 1.65, 1.0 },
		{0.3,  0.0   ,	0.0  , 	0.0 , 0.85, 0.9 , 0.92, 0.4 },
		{0.35, 0.0   ,	0.0  , 	0.0 , 1.2 , 0.75, 0.3 , 0.35},
		{0.43, 0.0   ,	0.0  , 	0.0 , 1.2 , 0.3 , 0.6 , 0.3 }
};

};
#endif /* L5_APPLICATION_MAPPING_HPP_ */
