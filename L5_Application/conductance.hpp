/*
 * salinity.hpp
 *
 *  Created on: Nov 10, 2015
 *      Author: Abhishek Gurudutt
 */

#ifndef L5_APPLICATION_CONDUCTANCE_HPP_
#define L5_APPLICATION_CONDUCTANCE_HPP_

#include "stdint.h"
#include "math.h"

// function to init the adc for reading conductance
bool initConductanceADC();

// function to blink the lights.
void blinkLightOnCalculating();

// function to switch off all lights
void switchOffLightsReset();

// function to switch on all lights once calculated,
void switchOnLightsAfterCalc();

// function to convert the adc values to voltage
float_t convertToVolt(uint16_t rawValue);

// function to read the raw values of conductance through adc.
uint16_t readConductance();

#endif /* L5_APPLICATION_CONDUCTANCE_HPP_ */
