/*
 * BLE.hpp
 *
 *  Created on: Nov 12, 2015
 *      Author: Abhi
 */

#ifndef L5_APPLICATION_BLE_HPP_
#define L5_APPLICATION_BLE_HPP_

#include "math.h"
#include "uart2.hpp"

class bluetooth : public SingletonTemplate<bluetooth>
{
public:
	// function to send a string to bluetooth module, accepts a string
	bool sendToBLE(char *string);

	// Initialize the bluetooth module
	bool initBLE();

	// Function to receive messages through bluetooth, returns a char.
	char recieveFromBLE();

	// function to send a conductance value through bluetooth
	bool sendCond(float_t cond);

	// function to send a temperature value through bluetooth
	bool sendTemp(float_t temp);

	// function to send salinity level through bluetooth
	bool sendSalinity(float_t salt);

	// function to send a char through bluetooth.
	bool sendChar(char sendChar);

private:
	bluetooth();
	Uart2 &BLE = Uart2::getInstance();
	friend class SingletonTemplate<bluetooth>;
};
#endif /* L5_APPLICATION_BLE_HPP_ */
