/*
 * temperatureADC.hpp
 *
 *  Created on: Nov 30, 2015
 *      Author: Tejeshwar
 */

#ifndef L5_APPLICATION_TEMPERATUREADC_HPP_
#define L5_APPLICATION_TEMPERATUREADC_HPP_

#include "lpc_sys.h"
#include "lowPassFilter.hpp"
#include "stdint.h"
#include "math.h"

class tempADC : public LPC_Filter
{
	public:
		void adc_init();
		uint16_t adc_compute();
		float_t converToTemp(uint16_t rawTemp);

	private:
		uint16_t offset = 0;
		uint16_t offset1 = 4096;
		float_t temp_coeff = 16;
		float_t actual_temperature = 0;
};



#endif /* L5_APPLICATION_SMARTTONGUEADC_HPP_ */





