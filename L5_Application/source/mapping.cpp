/*
 * mapping.cpp
 *
 *  Created on: Nov 30, 2015
 *      Author: Abhi
 */

#include "mapping.hpp"


float_t map3D::mapOutput(float_t input, float_t lowInput, float_t highInput, float_t lowOutput , float_t highOutput)
{

    float_t percentChange = (input - lowInput) / float_t (highInput - lowInput);

    float_t output = ((highOutput - lowOutput) * percentChange) + lowOutput;

    return output;
}

uint8_t map3D::getLowerRangeRowIndex(float_t value)
{
	uint8_t i = 0;

	for(i = 1; i < NUMBER_OF_TEMP; i++)
	{
		if(!(salinity_percent[i][0] < value))
		{
			break;
		}
	}

	return (i-1);
}

uint8_t map3D::getLowerRangeColumnIndex(float_t value)
{
	uint8_t i = 0;

	for(i = 1; i < NUMBER_OF_COND; i++)
	{
		if(!(salinity_percent[0][i] < value))
		{
			break;
		}
	}

	return (i-1);
}

float_t map3D::mappingFunc(float_t rowValue, float_t columnValue)
{
	uint8_t rowIndex, colIndex;
	float_t output1, output2, salt;

	rowIndex = getLowerRangeRowIndex(rowValue);
	colIndex = getLowerRangeColumnIndex(columnValue);

	// mapping the output for corresponding row value
	output1 = mapOutput(rowValue, salinity_percent[rowIndex][0], salinity_percent[rowIndex + 1][0],
			salinity_percent[rowIndex][colIndex], salinity_percent[rowIndex + 1][colIndex]);
	output2 = mapOutput(rowValue, salinity_percent[rowIndex][0], salinity_percent[rowIndex + 1][0],
			salinity_percent[rowIndex][colIndex + 1], salinity_percent[rowIndex + 1][colIndex + 1]);

	salt = mapOutput(columnValue, salinity_percent[0][colIndex], salinity_percent[0][colIndex + 1], output1, output2);

	return salt;
}
