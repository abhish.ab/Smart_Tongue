/*
 * Author: Abhishek Gurudutt
 *
 */

#include "LPC17xx.h"
#include <conductance.hpp>
#include "io.hpp"
#include "adc0.h"
#include "gpio.hpp"


#define MAXVOLT		3.3

bool initConductanceADC()
{
	LPC_PINCON->PINSEL3 |= (3<<30);
	return true;
}

void blinkLightOnCalculating()
{

	static int LED_no = 0;

	LED_no = (LED_no % 4) + 1;
    LE.toggle(LED_no);
}

uint16_t readConductance()
{
	uint16_t conductance;

	conductance = adc0_get_reading(5);

	return conductance;
}

float_t convertToVolt(uint16_t rawValue)
{
	float_t volt;

	volt = (rawValue / 4096.0) * MAXVOLT;

	return volt;
}

void switchOffLightsReset()
{
	static int LED_no = 0;

	LED_no = (LED_no % 4) + 1;
    LE.off(LED_no);
}

void switchOnLightsAfterCalc()
{
	static int LED_no = 0;

	LED_no = (LED_no % 4) + 1;
	LE.on(LED_no);
}
