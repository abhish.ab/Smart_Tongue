/*
 * temperatureADC.cpp
 *
 *  Created on: Nov 30, 2015
 *      Author: Tejeshwar
 */

#include "temperatureADC.hpp"
#include "lpc_sys.h"
#include "adc0.h"
#include "stdio.h"
#include "lowPassFilter.hpp"

#define testingCode 0

void tempADC::adc_init()
{
	LPC_PINCON->PINSEL3 |= (3<<28);//pin 28 & 29 = ADC0.4;
	adc0_init();
}

uint16_t tempADC::adc_compute()
{
	uint16_t temp = adc0_get_reading(4);
	return temp;
}
#if testingCode
	int temp = 2048;
#endif

float_t tempADC::converToTemp(uint16_t rawTemp)
{
	if(rawTemp<3000)
	{
		actual_temperature = (rawTemp - offset)/temp_coeff;
		//printf("%f\n",actual_temperature);
	}
	else if(rawTemp>=3000 && rawTemp<4096)
	{
		actual_temperature = (rawTemp - offset1)/temp_coeff;
		//printf("%f\n",actual_temperature);
	}
	else
	{
		//printf("out of range\n");
	}



	return actual_temperature;
	//filter(actual_temperature);
}



