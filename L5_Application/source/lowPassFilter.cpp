/*
 * lowPassFilter.cpp
 *
 *  Created on: Nov 30, 2015
 *      Author: Tejeshwar
 */

#include "lpc_sys.h"
#include "stdio.h"
#include "temperatureADC.hpp"
#include "lowPassFilter.hpp"
#include "utilities.h"

#define CONSISTENCY 1/1000.0  //0 to 0.001
//-0.001 to +0.001

/*
 * Low pass filter to soomthen the values.
 * Since, the variation in values are more we implemented this filter to get better results.
 */



float LPC_Filter::filter(float data, float *prevData)
{
	dataToFilter = data;
	*prevData = *prevData - (lpfBeta * (*prevData - dataToFilter));
	return *prevData;
	//printf("filteredData:%f\n",lpf_Data);
}

bool LPC_Filter::saltLevel(float saltData)
{
	compareVar = saltData - dataToBLE;
	dataToBLE = saltData;

	if(compareVar <= CONSISTENCY && compareVar >= -CONSISTENCY)
		return true;
	else
		return false;
}



