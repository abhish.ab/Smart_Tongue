/*
 * BLE.cpp
 *
 *  Created on: Nov 12, 2015
 *      Author: Abhi
 */

#include "io.hpp"
#include "BLE.hpp"
#include "stdio.h"



bool bluetooth::initBLE()
{
	return BLE.init(38400, 100, 100);
}

bool bluetooth::sendToBLE(char *string)
{
	return (BLE.put(string, 10));
}

char bluetooth::recieveFromBLE()
{
	char string;
	bool ok;

	ok = BLE.getChar(&string, 10);

	if(ok)
		return string;

	else
		return false;
}

bool bluetooth::sendTemp(float_t temp)
{
	char formatTemp[10];

	sprintf(formatTemp, "&%f C~", temp);

	return sendToBLE(formatTemp);
}

bool bluetooth::sendCond(float_t cond)
{
	char formatCond[10];

	sprintf(formatCond, "+%f V~", cond);

	return sendToBLE(formatCond);
}

bool bluetooth::sendSalinity(float_t salt)
{
	char formatSalt[10];

	sprintf(formatSalt, "$%f %%~", salt);

	return sendToBLE(formatSalt);
}

bool bluetooth::sendChar(char sendChar)
{
	char formatChar[4] = {0};
	sprintf(formatChar, "%c~", sendChar);

	return sendToBLE(formatChar);
}

bluetooth::bluetooth()
{
// do nothing
}
